BASE = __dir__

require "#{BASE}/lib/events.rb"
require "#{BASE}/lib/inline.rb"
require "#{BASE}/lib/header_link.rb"
require "#{BASE}/lib/external_link.rb"
require "#{BASE}/lib/fancy_image.rb"
require "#{BASE}/lib/highlighted_title.rb"
require "#{BASE}/lib/html_events.rb"
require "#{BASE}/lib/tt.rb"

helpers RawHelpers
helpers InlineAssets
helpers HeaderLink
helpers ExternalLink
helpers FancyImage
helpers HighlightedTitle
helpers HtmlEvents
helpers TT

set :css_dir, 'stylesheets'
set :js_dir, 'javascripts'
set :images_dir, 'images'
set :raw_dir, 'raw'

# Haml::TempleEngine.disable_option_validator!

# ===== END CODES DIRECTORY =====

activate :i18n
activate :directory_indexes

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :cache_buster

  # Compress PNGs after build
  # First: gem install middleman-smusher
  # require "middleman-smusher"
  # activate :smusher
end
