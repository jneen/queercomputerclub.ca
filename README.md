# Queer Computer Club website generator

## setup

* Install Ruby 3.2
* run `bundle`
* run the dev server with `bundle exec middleman`
* build the static files with `bundle exec middleman build`
