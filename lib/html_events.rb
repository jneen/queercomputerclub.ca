module HtmlEvents
  def html_events()
    events = get_events()
    if events.empty?
      return %|<div class="well"><p>There are no upcoming events at this time.</p></div>|.html_safe
    end
    events_html = events.map { |event| 
%|<div class="event" id="#{event.guid}">
  <p><b>#{event.title}</b>&nbsp;&nbsp;<date>#{event.date}</date> <time>#{event.time}</time></p>
  <p><i>#{event.location}</i></p>
  #{event.description}
</div>|
    }
    %|<div class="events">#{events_html.join(" ")}</div>|.html_safe
  end
end
