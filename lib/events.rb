require 'yaml'

Event = Struct.new(:date, :title, :location, :description, :time, :announced) do
  def guid
    Digest::SHA1.hexdigest("#{title} #{date} #{time}")
  end

  def datetime
    dt = Date.strptime(date, "%d-%m-%Y")
    Time.new(dt.year, dt.month, dt.day)
  end

  def announced_datetime
    dt = Date.strptime(announced, "%d-%m-%Y")
    Time.new(dt.year, dt.month, dt.day)
  end
end
def get_events()
  events_path = File.join(app.root, 'events.yaml')
  data = YAML.load(File.read(events_path))
  if data.nil?
    return []
  end
  data.map { |event_data| Event.new(*event_data.values_at('date', 'title', 'location', 'description', 'time', 'announced')) }.sort { |a, b| a.datetime <=> b.datetime }
end
