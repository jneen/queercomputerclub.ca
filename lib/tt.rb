module TT
  def tt(k)
    page_name = current_page.data&.page_name
    raise "set page_name in front matter" unless page_name
    t("#{page_name}/#{k}")
  end
end
