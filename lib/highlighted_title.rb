module HighlightedTitle
  def highlighted_title(text)
    %|<h1><span class="highlight">#{text}</span></h1>|.html_safe
  end
end
