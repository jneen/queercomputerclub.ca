require 'uri/http'

module ExternalLink
  def external_link(url, text)
    if url.start_with?("mailto:")
      link_image = "/images/icons/email_black.png"
    elsif url.start_with?("tel:")
      link_image = "/images/icons/phone.png"
    else
      uri = URI.parse(url)
      domain = PublicSuffix.parse(uri.host).domain
      link_image = case domain
        when "wikipedia.org"
          "/images/icons/wikipedia.png"
        when "youtube.com", "youtu.be"
          "/images/icons/youtube.png"
        else
          "/images/icons/link.png"
      end
    end
    text_start = text.partition(" ").first
    text_end = text.partition(" ").last
    if text_end != ""
      text_end = " " + text_end
    end
    return %|<a href="#{url}" target="_blank"><span style="display:inline-block;"><img class="external_link_icon" src="#{link_image}"/>#{text_start}</span>#{text_end}</a>|.html_safe
  end
end
