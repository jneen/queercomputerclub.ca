require 'base64'

module RawHelpers
  def inline_raw(name, fname)
    raw = File.read("./source/raw/#{fname}")
    b64 = Base64.strict_encode64(raw)

    content_tag :script, type: 'text/javascript' do
      "RAW__#{name} = '#{b64}'".html_safe
    end
  end
end

module InlineAssets
  def js(*sources)
    sources.map do |source|
      "<script type='text/javascript'>#{inline(source)}</script>"
    end.join("\n")
  end

  def css(*sources)
    sources.map do |source|
      raw_css(inline(source))
    end.join("\n")
  end

  def raw_css(source)
    "<style type='text/css'>#{source}</style>"
  end

  def img(name, attrs={})
    attrs = attrs.map { |k,v| "#{k}=#{v.inspect}" }.join(' ')
    %|<img src="#{image_data(name)}" #{attrs}/>|
  end

  def sfx(icon, sound)
    <<~HTML
      <span class="sfx">
        #{img(icon)}
        <audio src="/sounds/#{sound}"></audio>
      </span>
    HTML
  end

  def image_data(fname)
    fname = "./source/images/#{fname}"
    raw = File.read(fname)
    b64 = Base64.strict_encode64(raw)
    fname =~ /[.]([^.]+?)\z/

    ext = $1
    "data:image/#{ext};base64,#{b64}"
  end

  def icon_css(name, scale=1)
    fname = "./source/images/Icon/#{name}.png"
    png = ChunkyPNG::Image.from_file(fname)
    width = png.width * scale
    height = png.height * scale

    <<~CSS
    .icon.#{name} {
      width: #{width}px;
      height: #{height}px;
      background-image: url('/images/Icon/#{name}.png');
    }
    CSS
  end

  def inline(source)
    resource = sitemap.resources.find { |res| res.path == source }
    raise "could not find resource #{source}" if resource.nil?
    resource.render
  end
end

