module FancyImage
  def fancy_image(src, alt)
    %|<div class="fancy-image"><img src="#{src}" alt="#{alt}"><div class="tint-bg"></div><div class="tint-fg"></div></div>|.html_safe
  end
end
