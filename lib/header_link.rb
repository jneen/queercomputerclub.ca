module HeaderLink
  def header_link(path, text)
    if path == current_page.url
      %|<span class="b">#{text}</span>|
    else
      %|<a class="b" href="#{path}">#{text}</a>|
    end.html_safe
  end
end
