xml.instruct!
xml.urlset 'xmlns' => "http://www.sitemaps.org/schemas/sitemap/0.9" do
  sitemap.resources.select{ |page| page.data.title }.each do |page|
    xml.url do
      xml.loc URI.join("https://queercomputerclub.ca", page.destination_path.sub(/^index.html$|\/index.html$/, '/'))
    end
  end
end