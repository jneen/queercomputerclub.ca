const $ = document.querySelector.bind(document)
const $$ = document.querySelectorAll.bind(document)

colors = [
	["#F88BE2", "#9AF68C"],
	["#F88BE2", "#89FFFE"],
	["#FFA7ED", "#FFEE6A"],
	["#74B0FD", "#FFEE6A"],
	["#68BD5B", "#FFEE6A"],
	["#AB81D5", "#E8A2E3"],
	["#FF8080", "#9CFFD1"],
	["#FF8080", "#FFEE6A"],
	["#FF7EA2", "#89FFFE"],
]
color = colors[Math.floor(Math.random() * colors.length)];

html = $("html");
html.style.setProperty('--bg-background', color[0]);
html.style.setProperty('--bg-foreground', color[1]);

document.addEventListener("DOMContentLoaded", () => {
	$$("nav a.b").forEach(el => {
		el.onclick = () => {
			el.classList.add("clicked");
		}
	});
});