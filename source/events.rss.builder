xml.rss('version' => "2.0") do
  xml.channel do
    currtime = Time.now.strftime('%a, %d %b %Y %H:%M:%S %z')
    xml.title "Queer Computer Club - Events"
    xml.link "https://queercomputerclub.ca/events/"
    xml.description "Upcoming events at QCC"
    xml.pubDate currtime
    xml.lastBuildDate currtime
    xml.item do
      xml.title "Queer Computer Club's events RSS feed is deprecated"
      xml.link "https://queercomputerclub.ca/events/"
      xml.description "<p>We've deprecated our events RSS feed, please subscribe via our <a href=\"https://calendar.zoho.com/group/ical/zz080112306f87e66766f9ee80ec725f0ade6cc012b9363b49f75eb37caa1dc8f8f0b25e5aa544bbfc0d794f10ff30a18746bf125f\">ICAL feed</a> instead.</p><p>Adding this to your calendar will vary depending on the application you use, but be sure not to just download the file and import it. Your app should have a way for you to provide the link so it can frequently check for updates.</p>".html_safe
      xml.author "Directors"
      xml.pubDate Time.new(2024, 07, 12, 19, 50, 0).strftime('%a, %d %b %Y %H:%M:%S %z')
      xml.guid "24610aed-e33e-4d73-a7c7-5f8c183c3c81"
      xml.source "https://queercomputerclub.ca/events.rss"
    end
  end
end