---
title: Code of Conduct | Queer Computer Club
description: Code of Conduct of the Queer Computer Club
path: /code-of-conduct
page_name: Code of Conduct
---

#{highlighted_title 'Code of Conduct'}

%p(style="margin-top: -1em")
  %i Version 2.0 - August 2024

%p Your visit and membership to the Queer Computer Club is conditional on your compliance with our code of conduct. These rules are in place to ensure we create a welcoming and inclusive environment for all, especially queer, marginalized, and disabled people.

%p Members and visitors of the Queer Computer Club are expected to treat each other with patience, respect, and courtesy. Assume others are acting in good-faith. Do not assume people here are out to get you or hurt you, and in return, treat people as you'd like to be treated.

%p We live in a shitty society, but check your biases at the door as much as you can. We will undoubtedly trip over ourselves with stereotypes, but try not to assume anything about someone based on an identifiable characteristic. We do not tolerate conservative, anti-queer, or trans-exclusionary rhetoric. Additionally, we don't tolerate intolerance.

%p Conflict <em>will</em> happen. This is just a consequence of bringing many people together under one roof. Not everyone will get along all the time. If you feel slighted by someone, please remember to assume good-faith, and consider that they didn't legitimately want to hurt you. Conversely, if someone tells you that you slighted them, remember that they're not trying to destroy you. Simply apologize. "I'm sorry I made you feel uncomfortable" is all it takes. You will not be punished for sincerely apologizing.

%p Respect neurodivergence. Many folks are neuroatypical (i.e: autistic, ADHD, and so on) and many of us have trauma (often both). This means our way of relating to others can get complicated. Try to keep the phrase "you should've known better" out of your mind, because we're all at different stages of personal development.

%p Because this is a small space and it is often impossible not to eavesdrop, we ask that you avoid discussing heavy topics while others are present. This is not a space to air grievances, discuss internet drama, or to process traumas. Although we know that difficult discussions are often necessary, by default QCC is not the place to have them. These discussions are not conducive to the mission of a hackerspace; to be a place where people can learn from each other and participate in collaborative projects.

%p Discussing local politics is fine. It is ok to talk about Toronto news, upcoming elections, and similar topics. However, please be prudent about foreign news and politics. Unlike local politics, these are generally unactionable.

%p Words considered slurs are permitted, within appropriate context, and by those who have a claim. Don't use the ones you don't have a claim to (i.e: don't use the t-slur if you're not trans), and don't apply them to other people without permission. However, community identifiers (i.e: "queer community") are accepted, as many people do identify as being part of those communities.

%h2 Prohibited Behaviour

%h3 The following behaviour will not be tolerated:

%ul
  %li Offensive or bigoted comments about gender, race, class, sexuality, disability, physical appearance, body size, nationality, and religion.
  %li Deliberate intimidation, stalking, or following.
  %li Inappropriate physical contact.
  %li Non consensual exposure to sexual content.
  %li Threatening someone or a group of people verbally or physically.
  %li Unwelcome romantic or sexual attention.
  %li Deliberate misgendering or deadnaming.
  %li Deliberate, non consensual outing of someone's identity.

%p This behaviour is grounds for termination of membership and/or banning from the space.

%h3 The following behaviour is highly discouraged. If you do this, you will be asked to stop and apologize:

%ul
  %li Non consensual photography.
  %li General malicious behaviour, such as bullying, trolling, gatekeeping, playing "devil's advocate," name calling, or gossiping.
  %li Use of another person's computer or equipment without their permission.

%p Refusing to apologize or exhibiting a pattern of this behaviour is grounds for termination of membership and/or banning from the space.

%h3 Grievance Procedure

%p If you feel that someone has violated the code of conduct, please reach out to the directors at #{external_link "mailto:directors@queercomputerclub.ca", "directors@queercomputerclub.ca"}, or talk to one of us directly. Once we receive a report about someone, we will talk in private with the accused and figure out the best steps for remediation.

%h2 Social Guidelines:

%p The purpose of the Queer Computer Club is to cultivate a place for people to learn, grow, and collaborate. To this end, we have a few social guidelines we encourage you to follow to help create a positive and creative environment where everyone feels safe and welcome. These are not rules that are enforced on the threat of banning. These are conversation guardrails to help ensure that the atmosphere of the space remains productive. We ask that you do your best to follow them, and understand that you will receive gentle reminders if you violate them.

%p If you have any questions about these guidelines or want additional guidance, feel free to email #{external_link "mailto:directors@queercomputerclub.ca", "directors@queercomputerclub.ca"} or DM one of us on discord.

%h3 Stay on Topic

%p The purpose of the Queer Computer Club is to be a space where members of the queer community and our allies can share technical knowledge and skills, work on projects together, and make friends. Please understand that people come to the space and join the discord for these reasons, do not assume that everyone will be amenable to sensitive topics unrelated to art or tech.

%p QCC is not a community support group. A support group requires more resources, safety controls, and facilitation than we are able to provide, and it is ultimately not the mission of the Queer Computer Club organization.

%p Conversations about personal details, trauma, and venting should be avoided when among unfamiliar people. Collect consent before moving the conversation to these topics, and be especially aware when someone you don’t know very well is present. Also recognize that our space is small: any conversation will be overheard by everyone in the space. If there are multiple conversations happening at once, it’s not possible to collect consent from everyone. Same goes for the discord. Please end sensitive conversations when someone new enters the space midway-through.

%p On the flip side of this, understand that it is both your right and your responsibility to set boundaries on the kinds of conversations you want to be a part of. If someone veers into territory that causes you distress, please speak up and ask to change to a more on-topic conversation.

%p We’re not here to ban having a bad day, and we’re not here to ban getting support from friends you make at the space. It’s fine to briefly allude to a sensitive topic to give context to an unrelated, on-topic thing. The purpose of this guideline is to emphasize that if you do need help and you do need to vent, it’s often best to have those conversations outside the space.

%p If you are in need of immediate, direct support, please talk to a director and we will do our best to connect you with resources that could be helpful.

%h3 Follow the "Lucky Ten Thousand" Principle

%figure
  %a(href="https://xkcd.com/1053/" class="no-underline")
    %img(src="https://imgs.xkcd.com/comics/ten_thousand.png" alt="XKCD comic illustrating the Lucky Ten Thousand principle. Description is listed below.")

%details
  %summary Image description
  %p
    [Caption above the panel:]
    %br
    I try not to make fun of people for admitting they don't know things.
    %br
    [Caption right below said caption:]
    %br
    Because for each thing "everyone knows" by the time they're adults, every day there are, on average, 10,000 people in the US hearing about it for the first time.
    %br
    [A list of equations.]
    %br
    Fraction who have heard of it at birth = 0%
    %br
    Fraction who have heard of it by 30 ≈ 100%
    %br
    US birth rate ≈ 4,000,000/year
    %br
    Number hearing about it for the first time ≈ 10,000/day
    %br
    [Caption above the next panel:]
    %br
    If I make fun of people, I train them not to tell me when they have those moments.
    %br
    And I miss out on the fun.
    %br
    [Megan is standing. Cueball is walking, with his palm out.]
    %br
    Megan: "Diet Coke and Mentos thing"? What's that?
    %br
    Cueball: Oh man! Come on, we're going to the grocery store.
    %br
    Megan: Why?
    %br
    Cueball: You're one of today's lucky 10,000.

%p Sometimes, a person will admit they don't know something that you think is obvious or basic knowledge. This may be surprising, but avoid expressing that surprise. This has the tendency to make the person feel put-down that they don't know this thing. Instead, try to jump into "teaching mode," and recognize that this is a fun opportunity to show someone something new to them.

.well
  %p
    <b>You:</b> If you want to get the number out of this string, you can use a regex.
    %br
    <b>Them:</b> What's a regex?

.well.well-light-red
  %p <b>Instead of saying</b> "What? You know Perl but don't know what regular expressions are? That's really weird, I thought that was like, Perl 101."

.well.well-light-green
  %p <b>Please try</b> "Regex means regular expression. It's a way to specify a pattern to match part of a string. I can show you..."

%h3 Compliment First, Critique Later

%p Making something is always an emotional commitment. When people share their creations, they are not always looking for critique. Sometimes they just want to share something with you.

%p Therefore, your first response shouldn't be to critique. Instead, try to focus on the parts you like, parts that must've taken a lot of effort, and give a compliment or two. Even if you don't have anything particularly nice to say, verbally acknowledging their effort communicates that you're approaching the conversation in good faith. After that, you should always ask if the person wants critique or not, and only then should you offer it.

%p Of course, this guideline does not apply if the person asked for critique directly.

.well
  %p
    <b>Them:</b> Hey check out this puzzle game I've been prototyping!

.well.well-light-red
  %p <b>Instead of saying</b> "I don't really understand the control scheme. It's very confusing, maybe limit it to fewer buttons. Also, the learning curve for the puzzles is too steep, I think. The art for the character is hella cute though. Plus the settings menu is really nice."

.well.well-light-green
  %p <b>Please try</b> "I really like the character art! It's really cute. Also, this settings menu is quite polished. I have some opinions about the control scheme and learning curve, though, if you're interested."

%h3 Don't "Well, actually…"

%p A "Well, actually…" is when you correct a minor, tangentially related mistake when someone else is talking. These corrections are generally not very helpful and have a tendency to derail the conversation. They are best avoided.

%p This guideline doesn't apply to relevant and necessary corrections, i.e. when you're directly helping someone learn something new.

.well
  %p
    <b>Them:</b> In order to optimize the speed, I've cached the hard drive fetches into an array.

.well.well-light-red
  %p <b>Avoid</b> "But your computer doesn't have a hard drive, it has a solid state drive."

%h3 Don't Malign Tool Choices

%p Sometimes someone will be using software or hardware you personally take issue with. Although you are allowed to have your own opinion, vocally maligning these tools is generally unhelpful. It's best to keep these opinions to yourself unless asked.

.well
  %p
    <b>Them:</b> I recently got a new macbook in order to start programming ruby.

.well.well-light-red
  %p <b>Avoid</b> "Those computers are so overpriced. Also, ruby? I've had so many bad experiences with ruby."
